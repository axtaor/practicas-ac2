# Como instalar el Quartus sin que pete todo

## Docker
Ahora se puede ejecutar directamente usando Docker!! 
*(Cuidado, la imagen pesa unos 5.13GB y se descargará la primera vez que ejecutes)*
```bash
wget https://gitlab.com/axtaor/practicas-ac2/raw/master/exe_quartus.sh
chmod +x exe_quartus.sh 
./exe_quartus.sh
```
*Probado en Arch usando gnome 3.34 con wayland, si no funcionase algo abre una issue en el repo con el error.*

Docker montará la carpeta en la que ejecutes `exe_quartus.sh` en `/home/usuari`.

## Método antiguo (Puede no funcionar)
_Guia realizada usando Ubuntu 18.04, debería servir para cualquier versión_

Instalamos librerías random de 32 bits:

```bash
sudo apt install libstdc++6:i386 libc6:i386 libx11-dev:i386 libxext-dev:i386 libxau-dev:i386 libxdmcp-dev:i386 libfreetype6:i386 fontconfig:i386 expat:i386 libc6-dev-i386
```

Descargamos el Quartus en una carpeta temporal:

```bash
cd /tmp
mkdir installer_quartus
cd installer_quartus
wget http://www.hs-augsburg.de/~beckmanf/public/Quartus-web-13.0.1.232-linux.tar
tar -xvf Quartus-web-13.0.1.232-linux.tar
```

Procedemos a instalar usando el script que nos dan:

```ash
./setup.sh
```

Se abre el instalador, proceder con las opciones por defecto modificando el directorio de instalación si se quiere.

Para que se pueda abrir el quartus, hay que instalar una verión antigua de 32 bits de `libpng`:

```bash
wget http://security.ubuntu.com/ubuntu/pool/main/libp/libpng/libpng12-0_1.2.54-1ubuntu1.1_i386.deb
sudo dpkg -i libpng12-0_1.2.54-1ubuntu1.1_i386.deb
```

Por alguna razón no la pilla bien, así que hay que copiarla a la raiz de `/lib`

```bash
sudo cp /lib/i386-linux-gnu/libpng12.so.0.54.0 /lib/libpng12.so.0
```

Ahora viene lo interesante: con todo loque hemos instalado el quartus va pero el modelsim peta, así que hay que descargarse y compilar una versión antigua de `libfreetype`

```bash
cd /tmp
sudo apt-get build-dep -a i386 libfreetype6
wget http://download.savannah.gnu.org/releases/freetype/freetype-2.4.12.tar.bz2
tar -xjvf freetype-2.4.12.tar.bz2
cd freetype-2.4.12
./configure --build=i686-pc-linux-gnu "CFLAGS=-m32" "CXXFLAGS=-m32" "LDFLAGS=-m32"
make -j4
```

Ahora copiamos la librería a algún lugar decente (en este caso, dentro de la carpeta de instalación del quartus):

```bash
cd /carpeta/donde/has/instalado/el/quartus/modelsim_ase/
mkdir lib32
cp /tmp/freetype-2.4.12/objs/.libs/libfreetype.so* ./lib32
```

Finalmente podemos ejecutar el quartus usando estos comandos:

```bash
export LD_LIBRARY_PATH=/carpeta/donde/has/instalado/el/quartus/modelsim_ase/lib32
/carpeta/donde/has/instalado/el/quartus/bin/quartus
```
