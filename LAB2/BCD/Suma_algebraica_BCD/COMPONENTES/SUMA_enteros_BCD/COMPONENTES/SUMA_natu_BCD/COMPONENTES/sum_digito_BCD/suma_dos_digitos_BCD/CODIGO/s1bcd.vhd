--
-- Copyright (c) 2018, UPC
-- All rights reserved.
-- 

library ieee;
use ieee.std_logic_1164.all;
use work.cte_tipos_bcd_pkg.all;
use work.componentes_digito_bcd_pkg.all;

entity s1bcd is
port (X: in st_bcd;
	Y: in st_bcd;
	cen: in std_logic;
	S: out st_bcd;
	csal: out std_logic);
end s1bcd;

architecture compor of s1bcd is 
-- senyales
signal sumaparc, c1x6: st_bcd;
signal entrada9: st_bcd_mas_1;
signal sal0: std_logic;
begin
	snbits0: snbits port map(X=>X, Y => Y,cen=>cen, sum=> sumaparc, csal=>sal0);
	entrada9 <= sal0 & sumaparc;
	mayor90: mayor9 port map(X => entrada9, S=>c1x6, csal=>csal);
	
	snbits1: snbits port map(X=>sumaparc, Y => c1x6, cen=>'0', sum=> S);

end;

