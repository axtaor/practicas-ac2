--
-- Copyright (c) 2018, UPC
-- All rights reserved.
-- 

library ieee;
use ieee.std_logic_1164.all;
use work.cte_tipos_bcd_pkg.all;
use work.componentes_snBCD_pkg.all;

entity sentBCD is
port (a: in st_ndig_bcd_mas_1;
	b: in st_ndig_bcd_mas_1;
	cen: in std_logic;
	s: out st_ndig_bcd_mas_1;
	irre: out std_logic );
end sentBCD;

architecture estructural of sentBCD is
--senyales
	signal sig_s, sig_a, sig_b, sum_csal: std_logic;
begin
	sig_a <= a(num_bits_ndigitos);
	sig_b <= b(num_bits_ndigitos);
	
	snBCD0: snBCD port map(a => a(num_bits_ndigitos-1 downto 0),
	b => b(num_bits_ndigitos - 1 downto 0),
	cen => cen,
	s => s(num_bits_ndigitos - 1 downto 0),
	csal => sum_csal);
	
	s1bit0: s1bit port map(x => sig_a, y => sig_b, cen=> sum_csal, s => sig_s);
	
	s(num_bits_ndigitos) <= sig_s;
	irre <= ((not  sig_a) and (not sig_b) and sig_s) or (sig_a and sig_b and (not sig_s));

end estructural;

