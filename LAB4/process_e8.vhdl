calculainstrucc: process(reloj) is
variable v_opALUd: st_opALU;
variable v_opMDd: st_opMD;
variable v_opSECd: st_opSEC;
variable l: line;
variable total: integer := 0;
variable nloads: integer := 0;
variable nstores: integer := 0;
variable nsecin: integer := 0;
variable nseccond: integer := 0;
variable naritm: integer := 0;
begin
if reloj'event and reloj = '1' then
		if Pcero = '0' then
			v_opALUd := s_opALU'delayed(0 fs);
			v_opMDd := s_opMD'delayed(0 fs);
			v_opSECd := s_opSEC'delayed(0 fs);
			total := total + 1;
			if v_opMDd(4) = '1' then
				if v_opMDd(3) = '0' then
					nloads := nloads +1;
				else
					nstores := nstores +1;
				end if;
			elsif v_opSECd(3) = '1' then
				if v_opSECd(2 downto 0) = "011" then
					nsecin := nsecin +1;
				else
					nseccond := nseccond +1;
				end if;
			elsif v_opALUd(4) = '1' then
				naritm := naritm + 1;
			end if;
			write (l, string("Total: " & integer'image(total)&"SEC: "&to_hstring(v_opSECd)));
			writeline (output, l);
		end if;
end if;
if finaltraza'event and finaltraza = '1' then
	write (l, string("Fin de ejecucion:"&LF));
	writeline (output, l);
	write (l, string("Total: " & integer'image(total -1) &
	" Loads: " &integer'image(nloads)&
	" Stores: "&integer'image(nstores)&
	" Salt cond: "&integer'image(nseccond)&
	" Salt incond: "&integer'image(nsecin)&
	" Aritm: "&integer'image(naritm)));
	writeline (output, l);
end if;
end process calculainstrucc;
