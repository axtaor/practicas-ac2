--
-- Copyright (c) 2018, UPC
-- All rights reserved.
-- 

library ieee;
use ieee.std_logic_1164.all;

use work.param_disenyo_pkg.all;
use work.cte_tipos_deco_camino_pkg.all;
use work.componentes_control_seg_pkg.all;
use work.retardos_cntl_seg_pkg.all;

entity LDD is
	port(IDL1, IDL2 : in dir_reg; 
		valIDL1, valIDL2 : in std_logic;
		rdA, rdM, rdF, rdE : in dir_reg; 
		PBRA, PBRM, PBRF, PBRE : in st_PBR;
		IDL1A, IDL1M, IDL1F, IDL1E: out std_logic;
		IDL2A, IDL2M, IDL2F, IDL2E: out std_logic);
end LDD;


architecture estructural of LDD is

type t_reg_font is array (1 to 2) of dir_reg;	-- L1 L2
type t_reg_dest is array (1 to 4) of dir_reg;	-- A M F E
signal reg_font: t_reg_font;
signal reg_dest: t_reg_dest;
type tip_valid is array (1 to 2) of std_logic;
type tip_pbr is array (1 to 4) of std_logic;
signal validesa:  tip_valid;
signal pbr: tip_pbr;
signal es_zero: tip_valid;
signal no_es_zero: tip_valid;
signal id_reg_zero: dir_reg;
type t_mat_dep is array (1 to 2, 1 to 4) of std_logic;
signal dep: t_mat_dep;
signal iguals: t_mat_dep;

begin
	-- Entrades:
	-- valIDl1/2: validesa identificadors register
	-- IDL1/2: identificadors regfont
	-- rdK: identificadors regDesti (ALU, M, FMTL, ES)
	-- PBRk: Permisos banc de registres
	-- Sortides:
	-- IDLxk: Registre font X es trob a la etapa K
	
	reg_font	<= (1 => IDL1, 2 => IDL2);
	reg_dest <= (1 => rdA, 2 => rdM, 3 => rdF, 4 => rdE); 
	validesa <= (1 => VALIDL1, 2=> VALIDL2);
	pbr <= (1 => PBRA, 2 => PBRM, 3 => PBRF, 4 => PBRE);
	id_reg_zero <= (others => '0');
	GEN_LDD: for i in 1 to 2 generate
				cmp_zero: cmp port map(a => reg_font(i), b => id_reg_zero, ig => es_zero(i));
				
				GEN_ETAPA: for j in 1 to 4 generate
								cmp_et: cmp port map(a => reg_font(i), b => reg_dest(j), ig => iguals(i,j));
								dep(i,j) <= (not es_zero(i) )and iguals(i,j) and validesa(i) and pbr(j);
								end generate GEN_ETAPA;
				end generate GEN_LDD;
	
	
	
	--IDL1A <= '1' when valIDL1 = '1'  and PBRA = '1'  and rdA = IDL1 else '0' after retLDD;
	--IDL1M <= '1' when valIDL1 = '1'  and PBRM = '1'  and rdM = IDL1 else '0' after retLDD;
	--IDL1F <= '1' when valIDL1 = '1'  and PBRF = '1'  and rdF = IDL1 else '0' after retLDD;
	--IDL1E <= '1' when valIDL1 = '1'  and PBRE = '1'  and rdE = IDL1 else '0' after retLDD;
	--IDL2A <= '1' when valIDL2 = '1'  and PBRA = '1'  and rdA = IDL2 else '0' after retLDD;
	--IDL2M <= '1' when valIDL2 = '1'  and PBRM = '1'  and rdM = IDL2 else '0' after retLDD;
	--IDL2F <= '1' when valIDL2 = '1'  and PBRF = '1'  and rdF = IDL2 else '0' after retLDD;
	--IDL2E <= '1' when valIDL2 = '1'  and PBRE = '1'  and rdE = IDL2 else '0' after retLDD;
	
	IDL1A <= dep(1,1) after retLDD;
	IDL1M <= dep(1,2) after retLDD;
	IDL1F <= dep(1,3) after retLDD;
	IDL1E <= dep(1,4) after retLDD;
	IDL2A <= dep(2,1) after retLDD;
	IDL2M <= dep(2,2) after retLDD;
	IDL2F <= dep(2,3) after retLDD;
	IDL2E <= dep(2,4) after retLDD;

end estructural;
