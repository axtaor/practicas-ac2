# Prácticas AC2

[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://gitlab.com/alex.torregrosa/practicas-ac2/issues)
[![HitCount](http://hits.dwyl.io/alex.torregrosa/practicas-ac2.svg)](#)

Prácticas de AC2 de la fib QP2019

## Instalación de quartus:

[Instrucciones](/Instalaci%C3%B3n%20Quartus.md)
